<?php
include 'connection.php';

$ID = mysqli_real_escape_string($dbcon,$_POST['id']);
$password = mysqli_real_escape_string($dbcon,$_POST['password']);

$salt = 'konraimairunarak';
 $hash_password = hash_hmac('sha256', $password, $salt);

 $sql = "SELECT * FROM user WHERE ID =? AND password=?";
 $stmt = mysqli_prepare($dbcon,$sql);
 mysqli_stmt_bind_param($stmt,"ss",$ID,$hash_password);
 mysqli_execute($stmt);
$result_user =  mysqli_stmt_get_result($stmt);

if($result_user -> num_rows == 1){
	session_start();
	$row_user = mysqli_fetch_array($result_user,MYSQLI_ASSOC);
	$_SESSION['uID'] = $row_user['ID'];
	if ($row_user['status'] == "admin") {
		$_SESSION['is_admin'] = "admin";
		echo "<script type='text/javascript'>alert('ยินดีต้อนรับ');</script>";

	echo "<meta http-equiv='refresh' content='0;url=main_admin.php'>";
	}else{
		$_SESSION['is_member'] = "user";
		$_SESSION['ID'] = $row_user['ID'];
		echo "<script type='text/javascript'>alert('ยินดีต้อนรับ');</script>";

	echo "<meta http-equiv='refresh' content='0;url=index.php'>";

	}
}else{
	echo "<script type='text/javascript'>alert('ชื่อหรือรหัสผ่านไม่ถูกต้อง');</script>";

	echo "<meta http-equiv='refresh' content='0;url=login.php'>";
}
?>