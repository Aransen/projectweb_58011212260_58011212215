<?php
	session_start();

	include 'connection.php';

	$sql = "SELECT * FROM user WHERE status ='user'";
	$result = mysqli_query($dbcon, $sql);

?>
<!DOCTYPE html>
<html>
<head>
	<title>โชว์ข้อมูลสมาชิค</title>
</head>
<body>
	<table border="1px">
		<tr>
  			<td>ชื่อ-นามสกุล</td>
  			<td>ID</td>
  			<td>Email</td>
  			<td>เบอร์โทร</td>
  			<td>ที่อยู่</td>
  			<td>วันเกิด</td>
  			<td>สถานะ</td>
  		</tr>
		<?php
  			while ($row = mysqli_fetch_assoc($result)) {	
  		?>
  		<tr>

  			<td name><?php echo $row['name_lastname'] ?></td>
  			<td><?php echo $row['ID'] ?></td>
  			<td><?php echo $row['email'] ?></td>
  			<td><?php echo $row['Tel'] ?></td>
  			<td><?php echo $row['address'] ?></td>
  			<td><?php echo $row['bday'] ?></td>
  			<td><?php echo $row['status'] ?></td>
  		</tr>
  		<?php }?>
  	</table>
	<a href="main_admin.php"><span>กลับหน้าแรก</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
</body>
</html>