<?php
    session_start();
    include 'connection.php';
    $ID = $_GET['id'];
    $type = $_GET['type'];
    $sql = $sql = "SELECT * FROM game WHERE gid='$ID'";
    //$sql_join = $sql = "SELECT * FROM game INNER JOIN gametype ON game.type = gametype.typeID ORDER BY gid ASC";
    $result = mysqli_query($dbcon,$sql);
    //$result_join = mysqli_query($dbcon,$sql_join);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>JT GAMEs</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href="index.php"><img src="img/core-img/jtlogo.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="login-area">
                                <?php
                                    if(isset($_SESSION['is_member'])){
                                 ?>
                                 <a href="logout.php"><span>Logout</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php  }else{ ?>
                                <a href="login.php"><span>Login</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                 <a href="register.php"><span>Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php  }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">หน้าแรก</a></li>
                                    <li><a href="ShowAll_game.php">เกมส์ทั้งหมด</a>
                                                <?php 
                        if (isset($_SESSION['is_member'])) {
                        ?>
                                    <li><a href="contact.php">แจ้งโอนเงิน</a></li>
                                    <li><a href="update_dataUser.php">แก้ไขข้อมูล</a></li>
                                    <li><a href="showdata_user.php">แสดงข้อมูลสมาชิก</a></li>
                                    <li><a href="list_buy_game.php">ตะกร้า</a></li>
                          
                      <div class="uk-panel">
                        <p> 
                            ชื่อสมาชิก :<?php echo $_SESSION['ID']?>
                        </p>
                          
                      </div>
                        <?php } ?>
                        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/27.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Breadcrumb Text -->
                <div class="col-12">
                    <div class="breadcrumb-text">
                        <h2>ซื้อเกมส์</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Single Game Review Area Start ##### -->
    <?php
                                while ($row = mysqli_fetch_assoc($result)){
                                    ?>
    <section class="single-game-review-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single-game-img-slides">
                        <div id="gameSlides" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src=<?php echo $row['Gimage'];?> alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row align-items-center">
                <!-- *** Review Area *** -->
                <div class="col-12 col-md-6">
                    <div class="single-game-review-area style-2 mt-100">
                        <div class="game-content">
                            <span class="game-tag">ประเภทเกม    <?php echo $type;  ?></span>
                            <a href="single-game-review.php" class="game-title"><?php echo $row['gname'];  ?></a>
                            <label>ราคา     <?php echo $row['price'];  ?>       บาท</label><br><br>
                            <div class="download-rating-area">
                                <div class="download-area">
                                	<?php
                                    if(isset($_SESSION['is_member'])){
                                 ?>
                                   <a href="insert_order_game.php?name=<?= $row['gname']?>&prices=<?=$row['price'];?>"><img src="img/bg-img/addpro.png" alt=""></a>
                                <?php  }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <!-- ##### Single Game Review Area End ##### -->
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>
