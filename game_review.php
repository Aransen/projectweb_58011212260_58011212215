<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Egames - Gaming Magazine Template</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href="index.php"><img src="img/core-img/jtlogo.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="Login-area">
                                <a href="Login.php"><span>Login</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                 <a href="Register.php"><span>Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="game-review.php">Games</a>
                                        <ul class="dropdown">
                                             <li><a href="game-review.php">All Games</a></li>
                                            <li><a href="single-game-review.php">Single Game Review</a></li>
                                            <li><a href="action.php">action</a></li>
                                            <li><a href="">spot</a></li>
                                            <li><a href="">adventure</a></li>
                                            <li><a href="">racing</a></li>
                                            <li><a href="">Shooting</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.php">Contact</a></li>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>

    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/27.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Breadcrumb Text -->
                <div class="col-12">
                    <div class="breadcrumb-text">
                        <h2>All Games</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Game Review Area Start ##### -->
    <section class="game-review-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <!-- *** Single Review Area *** -->
                    <div class="single-game-review-area d-flex flex-wrap mb-30">
                        <div class="game-thumbnail">
                            <img src="img/bg-img/28.jpg" alt="">
                        </div>
                        <div class="game-content">
                            <span class="game-tag">Adventure</span>
                            <a href="single-game-review.php" class="game-title">NFS 2018</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            <!-- Download & Rating Area -->
                            <div class="download-rating-area d-flex align-items-center justify-content-between">
                                <div class="download-area">
                                    <a href="#"><img src="img/core-img/app-store.png" alt=""></a>
                                    <a href="#"><img src="img/core-img/google-play.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- *** Single Review Area *** -->
                    <div class="single-game-review-area d-flex flex-wrap mb-30">
                        <div class="game-thumbnail">
                            <img src="img/bg-img/29.jpg" alt="">
                        </div>
                        <div class="game-content">
                            <span class="game-tag">Adventure</span>
                            <a href="single-game-review.php" class="game-title">Call of Duty</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            <!-- Download & Rating Area -->
                            <div class="download-rating-area d-flex align-items-center justify-content-between">
                                <div class="download-area">
                                    <a href="#"><img src="img/core-img/app-store.png" alt=""></a>
                                    <a href="#"><img src="img/core-img/google-play.png" alt=""></a>
                                </div>  
                            </div>
                        </div>
                    </div>

                    <!-- *** Single Review Area *** -->
                    <div class="single-game-review-area d-flex flex-wrap mb-30">
                        <div class="game-thumbnail">
                            <img src="img/bg-img/30.jpg" alt="">
                        </div>
                        <div class="game-content">
                            <span class="game-tag">Adventure</span>
                            <a href="single-game-review.php" class="game-title">Star Wars</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            <!-- Download & Rating Area -->
                            <div class="download-rating-area d-flex align-items-center justify-content-between">
                                <div class="download-area">
                                    <a href="#"><img src="img/core-img/app-store.png" alt=""></a>
                                    <a href="#"><img src="img/core-img/google-play.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- *** Single Review Area *** -->
                    <div class="single-game-review-area d-flex flex-wrap mb-30">
                        <div class="game-thumbnail">
                            <img src="img/bg-img/31.jpg" alt="">
                        </div>
                        <div class="game-content">
                            <span class="game-tag">Adventure</span>
                            <a href="single-game-review.php" class="game-title">DOOM 2018</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            <!-- Download & Rating Area -->
                            <div class="download-rating-area d-flex align-items-center justify-content-between">
                                <div class="download-area">
                                    <a href="#"><img src="img/core-img/app-store.png" alt=""></a>
                                    <a href="#"><img src="img/core-img/google-play.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- *** Single Review Area *** -->
                    <div class="single-game-review-area d-flex flex-wrap mb-30">
                        <div class="game-thumbnail">
                            <img src="img/bg-img/34.jpg" alt="">
                        </div>
                        <div class="game-content">
                            <span class="game-tag">Adventure</span>
                            <a href="single-game-review.php" class="game-title">Destiny 2</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            <!-- Download & Rating Area -->
                            <div class="download-rating-area d-flex align-items-center justify-content-between">
                                <div class="download-area">
                                    <a href="#"><img src="img/core-img/app-store.png" alt=""></a>
                                    <a href="#"><img src="img/core-img/google-play.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ### Pagination Area ### -->
                    <nav aria-label="Page navigation example">
                        <ul class="pagination mt-100">
                            <li class="page-item active"><a class="page-link" href="#">01</a></li>
                            <li class="page-item"><a class="page-link" href="#">02</a></li>
                            <li class="page-item"><a class="page-link" href="#">03</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>