<?php
    session_start();
    include 'connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>JT GAMEs</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area wow fadeInDown" data-wow-delay="500ms">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href="index.php"><img src="img/core-img/jtlogo.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="login-area">
                                <?php
                                    if(isset($_SESSION['is_member'])){
                                 ?>
                                 <a href="logout.php"><span>Logout</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php  }else{ ?>
                                <a href="login.php"><span>Login</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                 <a href="register.php"><span>Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php  }?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">หน้าแรก</a></li>
                                    <li><a href="ShowAll_game.php">เกมส์ทั้งหมด</a>
                                                <?php 
                        if (isset($_SESSION['is_member'])) {
                        ?>
                                    <li><a href="contact.php">แจ้งโอนเงิน</a></li>
                                    <li><a href="update_dataUser.php">แก้ไขข้อมูล</a></li>
                                    <li><a href="showdata_user.php">แสดงข้อมูลสมาชิก</a></li>
                                    <li><a href="list_buy_game.php">ตะกร้า</a></li>
                          
                      <div class="uk-panel">
                        <p> 
                            ชื่อสมาชิก :<?php echo $_SESSION['ID']?>
                        </p>
                          
                      </div>
                        <?php } ?>
                        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Hero Area Start ##### -->
    <div class="hero-area">
        <!-- Hero Post Slides -->
        <div class="hero-post-slides owl-carousel">

            <!-- Single Slide -->
            <div class="single-slide bg-img bg-overlay" style="background-image: url(img/bg-img/1.jpg);">
                <!-- Blog Content -->
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12 col-lg-9">
                            <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                                <h2 data-animation="fadeInUp" data-delay="400ms">JT GAMEs</h2>
                                <p data-animation="fadeInUp" data-delay="700ms">ร้านขายเกมออนไลน์และเกมออฟไลน์</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Slide -->
            <div class="single-slide bg-img bg-overlay" style="background-image: url(img/bg-img/2.jpg);">
                <!-- Blog Content -->
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12 col-lg-9">
                            <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                                <h2 data-animation="fadeInUp" data-delay="400ms">JT GAMEs</h2>
                                <p data-animation="fadeInUp" data-delay="700ms">ร้านขายเกมออนไลน์และเกมออฟไลน์</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- ##### Monthly Picks Area Start ##### -->
    <section class="monthly-picks-area section-padding-100 bg-pattern">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="left-right-pattern"></div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Title -->
                    <h2 class="section-title mb-70 wow fadeInUp" data-wow-delay="100ms">เกมแนะนำ</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs wow fadeInUp" data-wow-delay="300ms" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="popular-tab" data-toggle="tab" href="#popular" role="tab" aria-controls="popular" aria-selected="true">เกมยอดฮิต</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content wow fadeInUp" data-wow-delay="500ms" id="myTabContent">
            <div class="tab-pane fade show active" id="popular" role="tabpanel" aria-labelledby="popular-tab">
                <!-- Popular Games Slideshow -->
                <div class="popular-games-slideshow owl-carousel">

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/55.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Grand Theft Auto V</a>
                            <div class="meta-data">
                              
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/51.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Doom</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/52.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">God of War</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/53.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Bloodborne</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/54.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Persona 5</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/52.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">God of War</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/53.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Bloodborne</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/54.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Persona 5</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="latest" role="tabpanel" aria-labelledby="latest-tab">
                <!-- Latest Games Slideshow -->
                <div class="latest-games-slideshow owl-carousel">

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/55.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Grand Theft Auto V</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/51.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Doom</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/52.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">God of War</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/53.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Bloodborne</a>
                            <div class="meta-data">
                               
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/54.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Persona 5</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/52.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">God of War</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/53.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Bloodborne</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/54.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Persona 5</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="editor" role="tabpanel" aria-labelledby="editor-tab">
                <!-- Editor Games Slideshow -->
                <div class="editor-games-slideshow owl-carousel">

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/55.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Grand Theft Auto V</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/51.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Doom</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/52.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">God of War</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/53.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Bloodborne</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/54.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Persona 5</a>
                            <div class="meta-data">
                               
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/52.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">God of War</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/53.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Bloodborne</a>
                            <div class="meta-data">
                                
                                <a href="#">Adventure</a>
                            </div>
                        </div>
                    </div>

                    <!-- Single Games -->
                    <div class="single-games-slide">
                        <img src="img/bg-img/54.jpg" alt="">
                        <div class="slide-text">
                            <a href="#" class="game-title">Persona 5</a>
                            <div class="meta-data">
                                
                                <a href="#">Action</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- ##### Monthly Picks Area End ##### -->
	
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>