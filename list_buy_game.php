<?php
session_start();
include 'connection.php';
$session_login_id = $_SESSION['uID'];
$sum = 0;
$sql = "SELECT oID,game_name,Oprice FROM game_order WHERE userName = '$session_login_id'";
    $result = mysqli_query($dbcon, $sql);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>JT GAMEs</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href="index.php"><img src="img/core-img/jtlogo.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="login-area">
                                <?php
                                    if(isset($_SESSION['is_member'])){
                                 ?>
                                 <a href="logout.php"><span>Logout</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php  }else{ ?>
                                <a href="login.php"><span>Login</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                 <a href="register.php"><span>Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php  }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">หน้าแรก</a></li>
                                    <li><a href="ShowAll_game.php">เกมส์ทั้งหมด</a>
                                                <?php 
                        if (isset($_SESSION['is_member'])) {
                        ?>
                                    <li><a href="contact.php">แจ้งโอนเงิน</a></li>
                                    <li><a href="update_dataUser.php">แก้ไขข้อมูล</a></li>
                                    <li><a href="showdata_user.php">แสดงข้อมูลสมาชิก</a></li>
                                    <li><a href="list_buy_game.php">ตะกร้า</a></li>
                          
                      <div class="uk-panel">
                        <p> 
                            ชื่อสมาชิก :<?php echo $_SESSION['ID']?>
                        </p>
                          
                      </div>
                        <?php } ?>
                        </div>
    </header>
    <!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
</body>
</html>
    <div class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/27.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Breadcrumb Text -->
                <div class="col-12">
                    <div class="breadcrumb-text">
                        <h2>ตะกร้า</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table border="1px">
        <tr>
            <td>ลำดับ</td>
            <td>เกมส์ที่ซื้อ</td>
            <td>ราคา</td>
 
        </tr>
        <?php
            while ($row = mysqli_fetch_assoc($result)) {    
        ?>
        <tr>
            <td name><?php echo $row['oID'] ?></td>
            <td><?php echo $row['game_name'] ?></td>
            <td><?php echo $row['Oprice'] ?></td>
            <td><a href="delete_Order_history.php?id=<?= $row['oID'];   ?>">ลบ</a></td>
          <?php $sum = $sum + $row['Oprice'];?>


        </tr>
        <?php }?>
    </table>
        <p>ราคารวมทั้งหมด    <?php echo $sum ?> บาท</p>
        <form action="contact.php">
            <input type="submit" value="แจ้งโอนเงิน">
        </form>

    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>