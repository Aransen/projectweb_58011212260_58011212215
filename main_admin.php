<?php
	session_start();

	include 'connection.php';

	$sql = "SELECT * FROM game INNER JOIN gametype ON game.type = gametype.typeID ORDER BY gid ASC";

	$res_news = mysqli_query($dbcon,$sql);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
  	<h1>ยินดีต้อนรับ</h1>
  	<p>คุณ <?php echo $_SESSION['uID']; ?></p>
  	<h1>เลือกเมนู</h1>
  	<p>
  		<a href="new_game.php">เพิ่มเกมส์</a>
      <a href="show_mass.php" name="data">ดูข้อมูลการสั่งซื้อ</a>
      <a href="update_dataAdmin.php" name="edit">แก้ไขข้อมูลส่วนตัว</a>
      <a href="ShowAll_data_user.php" name="data">ข้อมูลสมาชิค</a>
  	</p>
  	<table border="1px">
  		<tr>
  			<td>ลำดับ</td>
  			<td>ชื่อเกมส์</td>
  			<td>ประเภท</td>
  			<td>ราคา</td>
  			<td>แก้ไข</td>
  			<td>ลบเกมส์</td>
  		</tr>
  		<?php
  			while ($row_news = mysqli_fetch_assoc($res_news)) {	
  		?>
  		<tr>

  			<td name><?php echo $row_news['gid'] ?></td>
  			<td><?php echo $row_news['gname'] ?></td>
  			<td><?php echo $row_news['type'] ?></td>
  			<td><?php echo $row_news['price'] ?></td>
  			<td><a href="update_game.php?id=<?= $row_news['gid'];	?>">แก้ไข</a></td>
  			<td><a href="delete_game.php?id=<?= $row_news['gid'];	?>">ลบ</a></td>
  		</tr>
  		<?php }?>
  	</table>
	<a href="logout.php"><span>Logout</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
</body>
</html>