<?php
	session_start();

	include 'connection.php';
	$session_login_id = $_SESSION['uID'];

	$sql = "SELECT * FROM game_order INNER JOIN message ON game_order.oID = message.mID";
	$result = mysqli_query($dbcon, $sql);

?>
<!DOCTYPE html>
<html>
<head>
	<title>โชว์ข้อมูลสมาชิค</title>
</head>
<body>
	<table border="1px">
		<tr>
  			<td>ชื่อ-นามสกุล</td>
  			<td>เกมส์ที่ซื้อ</td>
  			<td>จำนวนเงินที่โอน</td>
  			<td>Email</td>
  			<td>ข้อความ</td>
  			<td>เวลาที่ส่ง</td>
  		</tr>
		<?php
  			while ($row = mysqli_fetch_assoc($result)) {	
  		?>
  		<tr>

  			<td name><?php echo $row['userID'] ?></td>
        <td name><?php echo $row['game_name'] ?></td>
        <td name><?php echo $row['price'] ?></td>
        <td name><?php echo $row['email'] ?></td>
        <td name><?php echo $row['mass'] ?></td>
        <td name><?php echo $row['time'] ?></td>
  			
  		</tr>
  		<?php }?>
  	</table>
	<a href="main_admin.php"><span>กลับหน้าแรก</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
</body>
</html>