<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Egames - Gaming Magazine Template</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href="index.php"><img src="img/core-img/jtlogo.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="login-area">
                                <a href="Login.php"><span>Login</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                 <a href="Register.php"><span>Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="game-review.php">Games</a>
                                        <ul class="dropdown">
                                            <li><a href="game-review.php">All Games</a></li>
                                            <li><a href="single-game-review.php">Single Game Review</a></li>
                                            <li><a href="action.php">action</a></li>
                                            <li><a href="sport.php">sport</a></li>
                                            <li><a href="adventure.php">adventure</a></li>
                                            <li><a href="racing.php">racing</a></li>
                                            <li><a href="shooting.php">Shooting</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.php">Contact</a></li>
                        </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/27.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Breadcrumb Text -->
                <div class="col-12">
                    <div class="breadcrumb-text">
                        <h2>Game Review</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Single Game Review Area Start ##### -->
    <section class="single-game-review-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single-game-img-slides">
                        <div id="gameSlides" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="img/bg-img/35.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="img/bg-img/36.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="img/bg-img/37.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="img/bg-img/38.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="img/bg-img/39.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="img/bg-img/40.jpg" alt="">
                                </div>
                            </div>
                            <ol class="carousel-indicators">
                                <li data-target="#gameSlides" data-slide-to="0" class="active" style="background-image: url(img/bg-img/35.jpg);"></li>
                                <li data-target="#gameSlides" data-slide-to="1" style="background-image: url(img/bg-img/36.jpg);"></li>
                                <li data-target="#gameSlides" data-slide-to="2" style="background-image: url(img/bg-img/37.jpg);"></li>
                                <li data-target="#gameSlides" data-slide-to="3" style="background-image: url(img/bg-img/38.jpg);"></li>
                                <li data-target="#gameSlides" data-slide-to="4" style="background-image: url(img/bg-img/39.jpg);"></li>
                                <li data-target="#gameSlides" data-slide-to="5" style="background-image: url(img/bg-img/40.jpg);"></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row align-items-center">
                <!-- *** Review Area *** -->
                <div class="col-12 col-md-6">
                    <div class="single-game-review-area style-2 mt-70">
                        <div class="game-content">
                            <span class="game-tag">Adventure</span>
                            <a href="single-game-review.php" class="game-title">Destiny 2</a>
                            <div class="game-meta">
                                <a href="#" class="game-date">July 12, 2018</a>
                                <a href="#" class="game-comments">2 Comments</a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            <!-- Download & Rating Area -->
                            <div class="download-rating-area">
                                <div class="download-area">
                                    <a href="#"><img src="img/core-img/app-store.png" alt=""></a>
                                    <a href="#"><img src="img/core-img/google-play.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Single Game Review Area End ##### -->
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>